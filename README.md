**Wallpapers**
****


# Here are some wallpapers that I have collected! Most of these are from either [imgur](https://imgur.com/) or found through the internet

If you see any wallpapers that are yours and want to be deleted please contact me via discord or twitter.


_**Style of wallpapers**_


The style of these wallpapers are usually either art, anime, a landscape or etc. I plan on putting more variety with these though.
